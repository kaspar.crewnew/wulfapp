import 'package:flutter/material.dart';
import 'package:wulfapp/app/landing_page.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(WulfApp());
}

class WulfApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WulfApp',
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
        fontFamily: 'Roboto', //TODO: Not working
      ),
      home: LandingPage(),
    );
  }
}
